# Define the cluster ecs
resource "aws_ecs_cluster" "app-cluster" {
  name = "app-cluster"
}

# Define the cluster_capacity_providers
resource "aws_ecs_cluster_capacity_providers" "capacity_providers" {
  cluster_name = aws_ecs_cluster.app-cluster.name

  capacity_providers = ["FARGATE"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

# Define the task definition
resource "aws_ecs_task_definition" "app-task-def" {
  family                   = "app-task"
  execution_role_arn       = aws_iam_role.ecs_tasks_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048
  container_definitions    = data.template_file.container-definition.rendered
}

# Define the container definition
data "template_file" "container-definition" {
  template = templatefile("templates/container_definition.json.tftpl", {
    container_name = "app_container"
    app_image      = var.app_image_name,
    app_port       = 80,
    fargate_cpu    = 1024,
    fargate_memory = 2048
    })
}

# Define the service
resource "aws_ecs_service" "app-service" {
  name            = "app-service"
  cluster         = aws_ecs_cluster.app-cluster.id
  task_definition = aws_ecs_task_definition.app-task-def.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.all_traffic.id] 
    subnets          = [aws_subnet.my_subnet.id]
    assign_public_ip = true

  }

  load_balancer {
  target_group_arn = aws_lb_target_group.app_target_group.arn
  container_name   = "app_container"
  container_port   = 80
  }

}
