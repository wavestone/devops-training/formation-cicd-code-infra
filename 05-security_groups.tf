resource "aws_security_group" "all_traffic" {
  name        = "all_traffic"
  description = "Allow all traffic"
  vpc_id      = aws_vpc.VPC.id

  ingress {
    description      = "all ingress"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"] 
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    description      = "all egress"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}